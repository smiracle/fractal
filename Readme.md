### Fractal

A Unity experiment I put together in 2017 to see how many rotating objects I could recursively render without a massive drop in framerate. The objects are randomly generated each time the application is loaded.